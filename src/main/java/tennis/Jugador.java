package tennis;

public class Jugador  {

    private int puntaje;

    public Jugador() {
        this.puntaje = 0;
    }

    public  void setPuntaje(int puntaje){
        this.puntaje = puntaje;
    }

    public int getPuntaje() {
        return puntaje;
    }
}
