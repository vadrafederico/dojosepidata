package tennis;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TennisTest {
    Jugador jugador1;
    Jugador jugador2;

    Partido partido;

    @Before
    public void init() {
        jugador1 = new Jugador();
        jugador2 = new Jugador();


        partido = new Partido(jugador1, jugador2);
    }

    @Test
    public void testConstructor(){
        assertEquals(jugador1, partido.getJugador1());
        assertEquals(jugador2, partido.getJugador2());
    }

    @Test
    public void aumentarPuntaje() {
        partido.aumentarPuntaje(jugador2);
        assertEquals(15, jugador2.getPuntaje());
    }

    @Test
    public void isDeuce() {
        this.jugador1.setPuntaje(40);
        this.jugador2.setPuntaje(40);

        Assert.assertTrue(partido.isDeuse());
    }

    @Test
    public void aumentar40() {
        this.jugador1.setPuntaje(40);
        this.jugador2.setPuntaje(40);
        this.partido.setSaque(jugador1);
        this.partido.aumentarPuntaje(jugador1);
        Jugador ganador = this.partido.getGanador();
        Assert.assertTrue(partido.termino());
        Assert.assertEquals(jugador1, ganador);
    }

    @Test
    public void aumentar40NoTermino() {
        this.jugador1.setPuntaje(40);
        this.jugador2.setPuntaje(40);
        this.partido.setSaque(jugador2);
        this.partido.aumentarPuntaje(jugador1);
        Assert.assertFalse(partido.termino());
    }


}
