package tennis;

import java.util.Random;

public class Partido {

    private Jugador jugador1;
    private Jugador jugador2;
    private Jugador saque;
    private boolean termino;
    private Jugador ganador;

    public Partido(Jugador jugador1, Jugador jugador2) {
        this.jugador1 = jugador1;
        this.jugador2 = jugador2;
        this.termino = false;
        this.saque = jugador1;

    }

    public Jugador getJugador1() {
        return jugador1;
    }

    public void setJugador1(Jugador jugador1) {
        this.jugador1 = jugador1;
    }

    public Jugador getJugador2() {
        return jugador2;
    }

    public void setJugador2(Jugador jugador2) {
        this.jugador2 = jugador2;
    }

    public void aumentarPuntaje(Jugador jugador) {
        switch (jugador.getPuntaje()) {
            case 0:
                jugador.setPuntaje((15));
                break;
            case 15:
                jugador.setPuntaje((30));
                break;
            case 30:
                jugador.setPuntaje((40));
                break;
            case 40:
                if (this.isDeuse()) {
                    if (saque.equals(jugador)) {
                        termino = true;
                        ganador = jugador;
                    }
                }

                break;
        }
    }

    public boolean isDeuse() {

        return this.jugador1.getPuntaje() == 40 && this.jugador2.getPuntaje() == 40;
    }


    public Jugador jugar(){
        Random random = new Random();
        while(!termino){
            int sacador = random.nextInt();
            if(sacador % 2 == 0){
                this.aumentarPuntaje(jugador1);
            } else {
                this.aumentarPuntaje(jugador2);
            }
        }

        return jugar();
    }


    public void setSaque(Jugador jugador1) {
        this.saque = jugador1;
    }

    public boolean termino() {
        return termino;
    }

    public Jugador getGanador() {
        return ganador;
    }


    public static void main(String [] args){

        Partido partido = new Partido(new Jugador(),new Jugador());
        partido.jugar();
    }
}
